# pikmin-fp-patch
An rdiff patch made using instructions from [Scruffy's First Person Pikmin video.](https://www.youtube.com/watch?v=RTrt3MVANpk)

Made from an ISO with an MD5 hash of 66f8d886afa0742cd9901d1bfe3b114f. If your ISO differs, it may cause issues.

This patch can be applied with this command: `rdiff patch your-iso pikdelta your-fancy-new-iso`
